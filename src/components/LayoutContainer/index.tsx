import * as React from 'react';
import SideNavContainer from '../SideNavigation';
import { Route, Switch } from "react-router";
import { NotesMain } from '../Notes/NotesMain';
import EditorContainer from '../EditorContainer';
import ArticlesContainer from '../ArticlesContainer';
import FullArticleContainer from '../FullArticleContainer';

export const LayoutContainer  = () => {
    return (
        <>
            <Switch>
                <Route path="/notes" component={NotesMain} />
                <Route path="/editor" component={EditorContainer} />
                <Route path="/articles/:id" component={FullArticleContainer} />
                <Route path="/articles" component={ArticlesContainer} />
            </Switch>
            <SideNavContainer />
        </>
    )
};