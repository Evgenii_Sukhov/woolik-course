import styled from 'styled-components';


const SideNav = styled.div`
    position: fixed;
    height: 100vh;
    right: 0;
    top: 0;
    width: 100px;
    background-color: ${props => props.theme.mainElemBg};
`;

const List = styled.ul`
    list-style: none;
    display: flex;
    height: 100%;
    flex-wrap: wrap;
    flex-direction: column;
`;

const ListItem = styled.li`
    display: block;
    text-align: center;
    margin-top: 30px;
    &.notification-item {
        position: relative;
        &>span {
            position: absolute;
            width: 17px;
            height: 17px;
            border-radius: 50%;
            color: #fff;
            background: #0087d0d9;
            bottom: 0;
            right: 19px;
            font-size: 12px;
            line-height: 17px;
        }
    }
    &>.ico {
        display: inline-block;
        height: 50px;
        width: 50px;
        opacity: 0.9;
        &.ico-setting:hover {
            animation: 1s ${props => props.theme.animations.roundRotation}, 0.5s ${props => props.theme.animations.fadeIn} linear forwards;
        }
        &.ico-notes:hover {
            animation: 0.4s ${props => props.theme.animations.zoom} linear forwards, 0.5s ${props => props.theme.animations.fadeIn} linear forwards;
        }
        &.ico-chat:hover, &.ico-bell:hover {
            animation: 1s ${props => props.theme.animations.waveRotation} linear forwards, 0.5s ${props => props.theme.animations.fadeIn} linear forwards;
        }
        &.ico-news:hover {
            animation: 1s ${props => props.theme.animations.rotateX} linear forwards, 0.5s ${props => props.theme.animations.fadeIn} linear forwards;
        }
        &.ico-exit:hover {
            animation: 1s ${props => props.theme.animations.rotateZ} linear forwards, 0.5s ${props => props.theme.animations.fadeIn} linear forwards;
        }
        &.ico-book:hover {
            animation: 1s ${props => props.theme.animations.rotateYSmall} linear forwards, 0.5s ${props => props.theme.animations.fadeIn} linear forwards;
        }
    }
    &:last-child {
        margin-top: auto;
        margin-bottom: 30px;
    }
`;

export {
    SideNav,
    List,
    ListItem
};