import styled from 'styled-components';


const ArticlesWrapper = styled.div`
    ${props => props.theme.components.pageWrapper}
`;

const ArticlesStyledContainer = styled.div`
    
    background-color: ${props => props.theme.mainElemBg};
    max-width: 800px;
    margin: auto;
    
    .content {
        padding: 20px;
    }
    
`;



export {
    ArticlesWrapper,
    ArticlesStyledContainer
}