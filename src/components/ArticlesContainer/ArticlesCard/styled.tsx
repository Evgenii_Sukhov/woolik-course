import styled from 'styled-components';

const Card = styled.div`
    background-color: ${props => props.theme.mainBg};
    margin-bottom: 20px;
    .card-header {
        padding: 10px 20px 0;
        font-weight: bold;
    }
    .card-body {
        padding: 20px 20px 0;
        display: flex;
        .card-preview {
            width: 150px;
            margin: 0 20px 0 0;
        }
    }
    .card-footer {
        padding: 20px;
        font-size: 14px;
        display: flex;
        justify-content: space-between;
        color: #828181;
    }
`;

export {
    Card
}