import * as React from 'react';
import { Card } from './styled';
import { IArticleItem } from '../../../core/types';
import { Link  } from 'react-router-dom';
import config from '../../../config.json';

export function ArticlesCard({articleItem}: {articleItem: IArticleItem}) {

    const parseTime= time => `${Math.ceil(time)} min read`;

    return (
        <Card>
            <div className="card-header">
                <Link to={
                    `/articles/${articleItem.uniqueSlug}`
                }>
                    {articleItem.title}
                </Link>
            </div>
            <div className="card-body">
                <div className="card-preview">
                    <img
                        src={`${config.server_api_url}/articles/preview/${articleItem.preview.metadata.id}`}
                    alt={articleItem.title} />
                </div>
                <div className="card-description">
                    {articleItem.subtitle || articleItem.title}
                </div>
            </div>
            <div className="card-footer">
                <div className="footer-date">
                    {articleItem.createdAtRow}
                </div>
                <div className="footer-time">
                    {parseTime(articleItem.readingTime)}
                </div>
            </div>
        </Card>
    )
}