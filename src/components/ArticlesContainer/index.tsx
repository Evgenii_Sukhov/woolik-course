import * as React from 'react';
import { ArticlesWrapper, ArticlesStyledContainer } from './styled';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import { getArticles } from '../../store/selectors/articles.selector';
import * as articlesActions from '../../store/actions/articles.actions';
import { IArticleItem } from '../../core/types';
import { ArticlesCard } from './ArticlesCard';

class ArticlesContainer extends React.Component<any, any>  {
    public componentDidMount() {
        this.props.getArticles();
    }

    public render() {
        return (
                <ArticlesWrapper>
                    <header>
                        <h1># Articles
                        </h1>
                    </header>

                        <ArticlesStyledContainer>
                            <Scrollbars style={{height: '80vh'}}>
                                <div className="content">
                                    {
                                        !this.props.articles.length || this.props.articles.map((e: IArticleItem) =>
                                            <div key={e.id}>
                                                <ArticlesCard articleItem={e} />
                                            </div>
                                        )
                                    }
                                </div>
                            </Scrollbars>
                        </ArticlesStyledContainer>

                </ArticlesWrapper>

        )
    }
}

const mapStateToProps = state => ({
    articles: getArticles(state)
});

const mapDispatchToProps = {
    getArticles: articlesActions.getArticles
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticlesContainer)