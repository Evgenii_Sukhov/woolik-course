import * as React from 'react';
import { List, ListItem, SideNav } from '../LayoutContainer/styled';
import { Link  } from 'react-router-dom';

export default class SideNavContainer extends React.Component {

   public render() {
       const unread = 1;
       return (
           <SideNav>
               <List>
                   <ListItem>
                       <Link className="ico ico-book" to="/homework" title="My Homework" />
                   </ListItem>
                   <ListItem>
                       <Link className="ico ico-owl" to="/articles" title="Articles" />
                   </ListItem>
                   <ListItem className="notification-item">
                       <a href="" className="ico ico-bell" title="Toggle Notification" />
                       <span>{unread}</span>
                   </ListItem>
                   <ListItem className="notification-item">
                       <a href="" className="ico ico-chat" title="Toggle Chat" />
                       <span>{unread}</span>
                   </ListItem>
                   <ListItem>
                       <Link className="ico ico-notes" to="/notes" title="My Notes" />
                   </ListItem>
                   <ListItem>
                       <Link className="ico ico-news" to="/news" title="My News" />
                   </ListItem>
                   <ListItem>
                       <Link className="ico ico-stat" to="/statistic" title="My Statistic" />
                   </ListItem>
                   <ListItem>
                       <Link className="ico ico-setting" to="/settings" title="My Settings" />
                   </ListItem>
                   <ListItem>
                       <Link className="ico ico-code-editor" to="/editor" title="Code editor" />
                   </ListItem>
                   <ListItem>
                       <Link className="ico ico-exit" to="/logout" title="Log Out" />
                   </ListItem>
               </List>
           </SideNav>
       )
   }
}