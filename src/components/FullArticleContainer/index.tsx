import * as React from 'react';
import { ArticlesWrapper, ArticlesStyledContainer } from './styled';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import { getArticles } from '../../store/selectors/articles.selector';
import * as articlesActions from '../../store/actions/articles.actions';

class FullArticleContainer extends React.Component<any, any>  {
    public componentDidMount() {
        this.props.getArticles();
    }

    public render() {
        return (
            <ArticlesWrapper>
                <header>
                    <h1># Articles
                    </h1>
                </header>

                <ArticlesStyledContainer>
                    <Scrollbars style={{height: '80vh'}}>
                        <div className="content">
                            Here
                        </div>
                    </Scrollbars>
                </ArticlesStyledContainer>

            </ArticlesWrapper>

        )
    }
}

const mapStateToProps = state => ({
    articles: getArticles(state)
});

const mapDispatchToProps = {
    getArticles: articlesActions.getArticles
};

export default connect(mapStateToProps, mapDispatchToProps)(FullArticleContainer)