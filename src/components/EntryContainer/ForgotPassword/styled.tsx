import styled from 'styled-components';

const ForgotPasswordContainer = styled.div`
    padding: 0 20px;
    color: ${props => props.theme.mainColor}
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    &>a {
        text-transform: uppercase;
        text-decoration: none;
        displayL inline-block;
        padding: 0 3%;
        background: #00000059;
        height: 41px;
        line-height: 41px;
    }
`;

const FieldSet = styled.fieldset`
    ${props => props.theme.components.fieldSetStyles}
    margin: 0;
    display: inline-block;
`;

const InputField = styled.input`
    ${props => props.theme.components.inputsStyles}
    width: 150px;
    height: 41px;
    margin: 0;
    &:focus + label,  &:valid + label {
        ${props => props.theme.components.labelStyles.focused}
    }
`;

const ButtonSubmit = styled.button`
    ${props => props.theme.components.buttonsStyles}
    width: 70px;
    height: 40px;
    line-height: 37px;
    padding: 0 3%;
    outline: none;
    border: 1px solid #ccc;
`;

const Label = styled.label`
    ${props => props.theme.components.labelStyles.general}
`;

export {
    ButtonSubmit,
    FieldSet,
    ForgotPasswordContainer,
    InputField,
    Label
}