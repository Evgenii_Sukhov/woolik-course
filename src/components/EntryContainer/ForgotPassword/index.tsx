
import * as React from 'react';
import { ButtonSubmit, FieldSet, ForgotPasswordContainer, InputField, Label } from './styled';

interface IForgotPassword {
    collapsed: boolean;
    toggleForm: () => void
}

export function ForgotPassword({collapsed, toggleForm}: IForgotPassword) {
    const toggleCollapsed = (event:React.MouseEvent<HTMLAnchorElement>) => {
        event.preventDefault();
        toggleForm();
    };

    function getForgotPasswordForm() {
        return (
            <form action="">
                <FieldSet>
                    <InputField id="put-code" required={true} />
                    <Label htmlFor="put-code">Enter login code</Label>
                </FieldSet>
                <ButtonSubmit>Send</ButtonSubmit>
            </form>

        )
    }

    return (
        <ForgotPasswordContainer>
            <a href="" onClick={toggleCollapsed}>Forget password</a>
            {!collapsed && getForgotPasswordForm()}
        </ForgotPasswordContainer>
    )
}