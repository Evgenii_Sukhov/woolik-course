import styled from 'styled-components';
import { device } from '../../core/Theme/devices';

const FormContainer = styled.div`
    position: absolute;
    top: 50%; left: 50%;
    transform: translate(-50%, -50%);
    background-color: ${props => props.theme.mainElemBg}
    @media ${device.mobileS} {
        width: 100%;
        padding-bottom: 20px;
    }
    
    @media ${device.laptop} {
        width: 400px;
        &:not(.login-container) {
            min-height: 65vh;
            max-height: 920px;
            padding-bottom: 0;
        }
        
    }
`;

const EntryFormHeading = styled.h2`
    background-color: ${props => props.theme.darkBgColor};
    padding: 20px;
    font-size: 140%;
    font-weight: 300;
    color: #fff;
`;

const Input = styled.input.attrs({
    type: 'text' || 'password' || 'phone' || 'tel'
})`
    ${props => props.theme.components.inputsStyles}
    font-size: 1em;
    margin-bottom: 0;
    &:focus + label,  &:valid + label {
        ${props => props.theme.components.labelStyles.focused}
    }
`;

const Button = styled.button`
    ${props => props.theme.components.buttonsStyles}
    margin-top: 30px;
    font-size: 1em;
    text-transform: uppercase;
`;

const FieldSet = styled.fieldset`
    ${props => props.theme.components.fieldSetStyles}
    &:not(:first-child) { margin-top: 40px; }
    .validation-error {
        font-size: 0.8em;
        color: ${props => props.theme.errorMsgColor};    
    }
    .validation-input-error {
        border-color: ${props => props.theme.errorMsgColor};
    }
`;

const Label = styled.label`
    ${props => props.theme.components.labelStyles.general}
    &:after {
        content: ' *';
        color: ${props => props.theme.errorMsgColor};
    }
`;

const Panel = styled.div`
    margin: 20px;
    padding: 20px 15px;
    font-size: 1.5em;
    text-align: center;
    color: ${props => props.theme.mainColor};
    background: ${props => props.theme.darkBgColor};
`;

export {
    FormContainer,
    EntryFormHeading,
    Button,
    FieldSet,
    Input,
    Label,
    Panel
}

