import * as React from 'react';
import { coursesActions } from '../../../store/actions/courses.actions';
import { getCourses } from '../../../store/selectors/courses.selectors';
import { connect } from 'react-redux';
import { IUserRegisterFields } from '../../../core/types';
import { getRegisterCode } from '../../../store/selectors/entry.selectors';
import { userActions } from '../../../store/actions/user.actions';
import { EntryFormHeading, FormContainer } from '../styled';
import Welcome from '../Welcome';
import { RegisterFormComponent } from '../RegisterForm';

interface IState extends IUserRegisterFields {
    validPassword: boolean,
    validBirthDate: boolean,
    formValid: () => boolean
}


class RegisterMainComponent extends React.Component<any, IState> {

    public state: IState = {
        birthDate: '',
        confirmPassword: '',
        courseID: [],
        email: '',
        firstName: '',
        lastName: '',
        password: '',
        phone: '',
        validPassword: true,
        validBirthDate: true,
        formValid: () => {
            return this.state.validPassword && this.state.validBirthDate
        }
    };

    public componentDidMount() {
        this.props.getCourseList();
    }

    public componentDidUpdate(prevProps) {
        if (!prevProps.code.length && this.props.code.length) {
            this.props.history.push('/login');
        }
    }

    public validateForm(name) {
        switch(name) {
            case 'password':
            case 'confirmPassword':
                this.setState({
                    validPassword: this.validatePasswords()
                });
                break;
            case 'birthDate':
                this.setState({
                    validBirthDate: this.validateBirthDate()
                })

        }
    }

    public validatePasswords() {
        const { confirmPassword, password } = this.state;
        if (confirmPassword.length) {
            return password === confirmPassword;
        }
        return true;
    }

    public handleSubmitRegister(e) {
        e.preventDefault();
        this.props.registerUser(this.state as IUserRegisterFields);
    }

    public handleChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        } as IState, () => this.validateForm(name));
    }

    public handleMultiSelectChange(value) {
        const courseID = [...value.map(e => e.courseID)];
        this.setState({
            courseID
        });
    }

    public render() {
        this.handleMultiSelectChange = this.handleMultiSelectChange.bind(this);
        this.handleSubmitRegister = this.handleSubmitRegister.bind(this);
        this.handleChange = this.handleChange.bind(this);
        const { courses } =  this.props;
        const { validPassword, validBirthDate, formValid } = this.state;
        return (
            <>
                <Welcome />
                <FormContainer className='register-container'>
                        <EntryFormHeading>
                            REGISTER NEW STUDENT
                        </EntryFormHeading>
                        <RegisterFormComponent
                            onChange={this.handleChange}
                            courses={courses}
                            validPassword={validPassword}
                            validBirthDate={validBirthDate}
                            formValid={formValid}
                            handleMultiSelectChange={this.handleMultiSelectChange}
                            handleSubmitRegister={this.handleSubmitRegister}
                        />
                </FormContainer>
            </>
        )
    }

    private validateBirthDate() {
        const pattern =/^(0[1-9]|[12][0-9]|3[01])[\- \/.](?:(0[1-9]|1[012])[\- \/.](19|20)[0-9]{2})$/;
        return pattern.test(this.state.birthDate.replace(/\s/g, ''));
    }
}

const mapStateToProps = state => ({
    courses: getCourses(state),
    code: getRegisterCode(state)
});

const mapDispatchToProps = {
    getCourseList: coursesActions.getCourseList,
    registerUser: userActions.registerUser
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterMainComponent);