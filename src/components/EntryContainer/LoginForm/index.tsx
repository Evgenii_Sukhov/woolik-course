import * as React from 'react';
import { Button, FieldSet, Input, Label } from '../styled';

export function LoginForm({onChange, onSubmit, code}) {
    return (
        <form onSubmit={onSubmit} className="centered-form" action="POST">
            <FieldSet>
                <Input name="code" value={code} type="text" id="code" required={true} onChange={onChange} />
                <Label htmlFor="code">Enter login code</Label>
            </FieldSet>
            <FieldSet>
                <Input name="password" type="password" id="password" required={true} onChange={onChange} />
                <Label htmlFor="password">Enter password</Label>
            </FieldSet>
            <Button type="submit">Sign in</Button>
        </form>
    )
}