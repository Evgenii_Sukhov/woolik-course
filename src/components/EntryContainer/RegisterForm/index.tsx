import * as React from 'react';
import { Link } from 'react-router-dom';
import { Button, FieldSet, Input, Label } from '../styled';
import { Scrollbars } from 'react-custom-scrollbars';
import InputMask from 'react-input-mask';
import Select from 'react-select'
import { SelectOption } from './styled';
import { StrengthPassword } from '../../../core/components/StrengthPassword';

export function RegisterFormComponent({
    onChange,
    handleMultiSelectChange,
    courses,
    validPassword,
    validBirthDate,
    formValid,
    handleSubmitRegister
}) {

    function Option({data, innerProps}) {
        const className = `select-ico ${data.className}`;
        return (
            <SelectOption {...innerProps}>
                <div className={className} />
                <div className="option-caption">{data.label}</div>
            </SelectOption>
        )
    }

    // const regExp = /^(0[1-9]|[12][0-9]|3[01])[\- \/.](?:(0[1-9]|1[012])[\- \/.](19|20)[0-9]{2})$/;


    const getTheme = theme => ({
        ...theme,
        borderRadius: 0,
        spacing: {
            ...theme.spacing,
            controlHeight: 45
        }
    });
    const getOptionValue = option => option.courseID;

    return (
        <Scrollbars style={{height: '75vh'}}>
            <form className="centered-form" action="POST" onSubmit={handleSubmitRegister}>
                <FieldSet>
                    <Input name="firstName" type="text" id="register-firsName" onBlur={onChange} required={true} />
                    <Label htmlFor="register-firsName">First name</Label>
                </FieldSet>
                <FieldSet>
                    <Input name="lastName" type="text" id="register-lastName" onBlur={onChange} required={true} />
                    <Label htmlFor="register-lastName">Last name</Label>
                </FieldSet>
                <FieldSet>
                    <Input name="email" type="email" id="register-email" onBlur={onChange} required={true} />
                    <Label htmlFor="register-email">Email</Label>
                </FieldSet>
                <FieldSet>
                    <InputMask mask="+38 (099) 999 - 99 - 99" maskChar=" " onBlur={onChange}>
                        {
                            () => <Input name="phone" type="phone" id="register-phone" required={true} />
                        }
                    </InputMask>
                    <Label htmlFor="register-phone">Phone</Label>
                </FieldSet>
                <FieldSet>
                    <InputMask mask="99 / 99 / 9999"
                               onBlur={onChange}>
                        {
                            () => <Input
                                className={!validBirthDate ? 'validation-input-error': ''}
                                name="birthDate" type="text"
                                id="register-date"
                                required={true} />
                        }
                    </InputMask>
                    <Label htmlFor="register-lastName">Birth date</Label>
                    {
                        !validBirthDate ? <div className="validation-error">Invalid birth date</div> : ''
                    }
                </FieldSet>
                <FieldSet>
                    <Select
                        isMulti={true}
                        placeholder="Choose course(s)"
                        options={courses.list}
                        closeMenuOnSelect={false}
                        className="register-courses"
                        menuPlacement="auto"
                        blurInputOnSelectboolean={false}
                        components={{Option}}
                        theme={getTheme}
                        getOptionValue={getOptionValue}
                        onChange={handleMultiSelectChange}
                    />
                </FieldSet>
                <FieldSet>
                    <StrengthPassword className={!validPassword ?'validation-input-error': ''} id="register-password" name="password"  onChange={onChange} />
                    <Label htmlFor="register-password">Password</Label>
                    {
                        !validPassword ? <div className="validation-error">Passwords don't match</div> : ''
                    }
                </FieldSet>
                <FieldSet>
                    <StrengthPassword className={!validPassword ?'validation-input-error': ''} id="register-confirm-password" name="confirmPassword" onChange={onChange} />
                    <Label htmlFor="register-confirm-password">Confirm Password</Label>
                    {
                        !validPassword ? <div className="validation-error">Passwords don't match</div> : ''
                    }
                </FieldSet>
                <Button type="submit" disabled={!formValid()}>Sign up</Button>
                <Link className="entry-navigation-link" to="/login">
                    <Button>Go to login page</Button>
                </Link>
            </form>
        </Scrollbars>
    )
}

