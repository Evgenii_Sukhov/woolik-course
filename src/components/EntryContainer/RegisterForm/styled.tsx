import styled from 'styled-components';

export const SelectOption = styled.div`
    ${props => props.theme.components.selectOptionStyles}
`;