import styled from 'styled-components';

const Heading = styled.h1`
    text-align: center;
    color: ${props => props.theme.mainColor};
    background: ${props => props.theme.mainElemBg};
    font-size: 2.5em;
`;


export {
    Heading
}