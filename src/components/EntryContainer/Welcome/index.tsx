import * as React from 'react';
import { Heading } from './styled';

export default function Welcome() {
    return <div>
        <Heading>Woolik Course App!</Heading>
        </div>
}