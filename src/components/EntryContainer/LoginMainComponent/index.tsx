import * as React from 'react';
import { Link } from 'react-router-dom';
import { LoginForm } from '../LoginForm';
import { ForgotPassword } from '../ForgotPassword';
import { connect } from 'react-redux';
import { Button, EntryFormHeading, FormContainer, Panel } from '../styled';
import Welcome from 'src/components/EntryContainer/Welcome';
import { getRegisterCode, getUserAuth } from '../../../store/selectors/entry.selectors';
import { WELCOME_TEXT } from '../../../core/text-constants';
import TypeWriterComponent from '../../../core/components/TypeWriter';
import { userActions } from '../../../store/actions/user.actions';

interface IState {
    code: string;
    collapsed: boolean;
    password: string;

}

class LoginMainComponent extends React.Component<any, IState>{

    public state: IState = {
        code: '',
        password: '',
        collapsed: true
    };

    public handleSubmit(e) {
        e.preventDefault();
        const {code, password} = this.state;
        this.props.loginUser({code, password});
    }

    public componentDidMount() {
        this.setState({
            code: this.props.code
        });
    }

    public componentDidUpdate(prevProps, prevState) {
        if(this.props.userAuth) {
            this.props.history.push('/');
        }
    }

    public toggleForm() {
        const { collapsed } = this.state;
        this.setState({
            collapsed: !collapsed
        });
    }

    public handleChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        } as IState);
    }

    public render() {
        const { code, collapsed } = this.state;
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.toggleForm = this.toggleForm.bind(this);


        return (
            <>
                <Welcome/>
                {
                    this.props.code ?
                        <Panel>
                            <TypeWriterComponent text={WELCOME_TEXT} />
                        </Panel> : ''
                }
                <FormContainer className="login-container">
                    <EntryFormHeading>
                        LOGIN TO YOUR ACCOUNT
                    </EntryFormHeading>
                    <LoginForm code={code} onSubmit={this.handleSubmit} onChange={this.handleChange}/>
                    <ForgotPassword collapsed={collapsed} toggleForm={this.toggleForm}/>
                    <div className="form-padding">
                        <Link to="/register">
                            <Button>Wanna register ?</Button>
                        </Link>
                    </div>
                </FormContainer>


            </>
        )
    }
}

const mapStateToProps = state => ({
    code: getRegisterCode(state),
    userAuth: getUserAuth(state)
});

const mapDispatchToProps = {
    loginUser: userActions.loginUser
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginMainComponent);