import styled from 'styled-components';

const EditorWrapper = styled.div`
    ${props => props.theme.components.pageWrapper}
`;

export {
    EditorWrapper
}