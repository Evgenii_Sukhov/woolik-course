import * as React from 'react';
import CodeEditor from '../../core/components/CodeEditor';
import * as monaco from 'monaco-editor';
import { EditorWrapper } from './styled';
import { CodeEditorControls } from '../../core/components/CodeEditor/CodeEditorControls';
import { Ilanguage, ITheme } from '../../core/types';
import { BASIC_THEMES } from '../../core/text-constants';
import Modal from '../../core/components/Modal';
import { CodeEditorOptions } from '../../core/components/CodeEditor/CodeEditorOptions';

export default class EditorContainer extends React.Component<any, any> {

    public state = {
        languages: this.getLanguages(),
        selectedLanguage: this.getLanguageById('javascript'),
        themes: BASIC_THEMES,
        selectedTheme: BASIC_THEMES[0],
        modalOpen: false,
        showCompilePanel: true,
        autoRun: true,
        processName: 'compiled'
    };

    public handleLanguageSelectChange(value: Ilanguage) {
        this.setState({selectedLanguage: value})
    }

    public onOpenCloseCallback(isOpen) {
        if (!isOpen) {
            this.setState({
                modalOpen: isOpen
            });
        }
    }

    public handleThemeSelectChange(value: ITheme) {
        this.setState({selectedTheme: value});
    }


    public render() {
        const {
            languages,
            selectedLanguage,
            selectedTheme,
            themes,
            modalOpen,
            showCompilePanel,
            autoRun, processName
        }= this.state;
        const options = {
            language: selectedLanguage.id,
            theme: selectedTheme.id
        };


        this.handleLanguageSelectChange = this.handleLanguageSelectChange.bind(this);
        this.handleThemeSelectChange = this.handleThemeSelectChange.bind(this);
        this.onClickOpenSettings  = this.onClickOpenSettings.bind(this);
        this.onOpenCloseCallback = this.onOpenCloseCallback.bind(this);
        this.handleShowCompileChange = this.handleShowCompileChange.bind(this);
        this.handleAutoRunChange = this.handleAutoRunChange.bind(this);
        this.onFinishManualCompile = this.onFinishManualCompile.bind(this);
        this.onClickCompileCode = this.onClickCompileCode.bind(this);
        return (
            <EditorWrapper>
                <header>
                    <h1>#Code Editor <span title="
                    Features:
                    -Support HTML;
                    -CSS;">
                        (v 0.1)
                    </span>
                    </h1>
                </header>
                <CodeEditorControls
                    onClickOpenSettings={this.onClickOpenSettings}
                    selectedLanguage={selectedLanguage}
                    showCompilePanel={showCompilePanel}
                    onClickCompileCode={this.onClickCompileCode}
                />
                <CodeEditor
                    options={options}
                    showCompilePanel={showCompilePanel}
                    autoRun={autoRun}
                    processName={processName}
                    onFinishManualCompile={this.onFinishManualCompile}
                />

                <Modal
                    open={modalOpen}
                    onOpenCloseCallback={this.onOpenCloseCallback}
                    backdrop={false}
                    position="center"
                    closeOnClickOutside={false}
                    modalTitle="Code Editor Settings"
                    width="400px"
                    draggable="true"
                >
                    <CodeEditorOptions
                        languages={languages}
                        autoRun={autoRun}
                        themes={themes}
                        selectedLanguage={selectedLanguage}
                        selectedTheme={selectedTheme}
                        showCompilePanel={showCompilePanel}
                        handleThemeSelectChange={this.handleThemeSelectChange}
                        handleLanguageSelectChange={this.handleLanguageSelectChange}
                        handleShowCompileChange={this.handleShowCompileChange}
                        handleAutoRunChange={this.handleAutoRunChange}
                    />
                </Modal>
            </EditorWrapper>
        )
    }

    private getLanguages(): Ilanguage[] {
        const languagesList: any[] = monaco.languages.getLanguages();
        return languagesList.map(elem => ({id: elem.id, label: elem.aliases[0]}))
    }

    private handleShowCompileChange(e) {
        this.setState({
            showCompilePanel: e.target.checked
        })
    }

    private onClickCompileCode() {
        if (!this.state.autoRun) {
            this.setState({processName: 'run'});
        }
    }

    private onFinishManualCompile() {
        this.setState({processName: 'compiled'});
    }

    private handleAutoRunChange(e) {
        this.setState({
            autoRun: e.target.checked
        })
    }

    private onClickOpenSettings() {
        this.setState({
            modalOpen: true
        })
    }

    private getLanguageById(id): Ilanguage {
        return this.getLanguages().find(e => e.id === id) as Ilanguage;
    }
}