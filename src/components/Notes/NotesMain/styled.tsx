import styled from 'styled-components';


const NotesWrap = styled.div`
    ${props => props.theme.components.pageWrapper}
`;

export {
    NotesWrap,
};