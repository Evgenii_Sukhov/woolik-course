import * as React from 'react';
import { NotesWrap } from './styled';
import { NotesCtrl } from '../NotesCtrl';
import { INotesState } from '../../../core/types';
import NotesList from '../NotesList';
import { Scrollbars } from 'react-custom-scrollbars';


export class NotesMain extends React.Component {
    public state: INotesState = {
        notes: [
            {
                body: 'sdfv fvb f dsfdf bgfhgf sfdgsfd sdfvdfvsdfv sflklf fvfvpl',
                tags: ['qq', 'ww'],
                title: 'note 1',
            },
            {
                body: 'sdfv fvb f dsfdf bgfhgf sfdgsfd sdfvdfvsdfv sflklf fvfvpl',
                tags: ['qq', 'ww'],
                title: 'note 1',
            },{
                body: 'sdfv fvb f dsfdf bgfhgf sfdgsfd sdfvdfvsdfv sflklf fvfvpl',
                tags: ['qq', 'ww'],
                title: 'note 1',
            },{
                body: 'sdfv fvb f dsfdf bgfhgf sfdgsfd sdfvdfvsdfv sflklf fvfvpl',
                tags: ['qq', 'ww'],
                title: 'note 1',
            },{
                body: 'sdfv fvb f dsfdf bgfhgf sfdgsfd sdfvdfvsdfv sflklf fvfvpl',
                tags: ['qq', 'ww'],
                title: 'note 1',
            },{
                body: 'sdfv fvb f dsfdf bgfhgf sfdgsfd sdfvdfvsdfv sflklf fvfvpl',
                tags: ['qq', 'ww'],
                title: 'note 1',
            },{
                body: 'sdfv fvb f dsfdf bgfhgf sfdgsfd sdfvdfvsdfv sflklf fvfvpl',
                tags: ['qq', 'ww'],
                title: 'note 1',
            },
            {
                body: 'sdfv fvb f dsfdf bgfhgf sfdgsfd sdfvdfvsdfv sflklf fvfvpl',
                tags: ['qq', 'ww'],
                title: 'note 1',
            }
        ]
    };
    public render() {
        return (
            <NotesWrap>
                <header>
                    <h1>#My notes</h1>
                    <NotesCtrl/>
                </header>
                <Scrollbars style={{ height: '80vh', width: '100%' }}>
                    <main>
                        <NotesList notes={this.state.notes}/>
                    </main>
                </Scrollbars>

            </NotesWrap>
        )
    }}
