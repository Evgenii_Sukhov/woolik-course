import * as React from "react";
import { CtrlWrap, Input, FieldSet, Label } from "./styled";
import DropDown from "../../../core/components/Dropdown";
import { IDDItem } from "../../../core/types";

export class NotesCtrl extends React.Component {
    public state: {items: IDDItem[], label: string, opened: boolean, opening: boolean}  = {
        items: [
            {
                default: true,
                text: 'All tags',
                value: 'all'
            },
            {
                default: false,
                text: 'Tag_1',
                value: 'Tag_1'
            },
            {
                default: false,
                text: 'Tag_2',
                value: 'Tag_2'
            },
            {
                default: false,
                text: 'Tag_3',
                value: 'Tag_3'
            },
        ],
        label: 'Sort by tags',
        opened: true,
        opening: false
    };

    public pickCallback = (i:IDDItem ) => {
        this.setState({
            ...this.state,
            label: i.text
        })
    };

    public toggleCtrl = () => {
        this.setState({
            ...this.state,
            opened: !this.state.opened,
            opening: !this.state.opened
        })
    };

    public render() {
        const {
            items,
            label,
            opened
        } = this.state;

        return (
            <CtrlWrap className={`${this.state.opened ? 'opened' : 'collapsed'} ${this.state.opening ? 'opening': ''}`}>
                <div onClick={this.toggleCtrl} className={`expander`}>
                    <div className={`ico ${opened ? 'ico-shrink' : 'ico-enlarge'}`}/>
                </div>
                <div className={'main'}>
                    <DropDown callback={this.pickCallback} items={items} label={label}/>
                    <FieldSet>
                        <Input id="search" className="search ico-search" type="text" required={true} />
                        <Label htmlFor="search">Search</Label>
                    </FieldSet>
                    <span className="ico ico-add"/>
                </div>
            </CtrlWrap>
        )
    }
}