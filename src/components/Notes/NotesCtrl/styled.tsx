import styled from 'styled-components';


const CtrlWrap = styled.div`
        ${props => props.theme.components.controlsWrapperStyles}
        color:  ${props => props.theme.shadowColor};
        background-color: ${props => props.theme.mainElemBg};
        
        &.opening {
            animation:  0.3s ${props => props.theme.animations.rize} linear forwards;
        }
        
        &.collapsed {
            transition: opacity 0.3s ease;
            position: fixed;
            animation:  0.3s ${props => props.theme.animations.collapse} linear forwards;
            overflow: hidden; 
            padding: 0;
            & .main {
                display: none;
            }
            
            & .expander {
                top: 3px;
                right: 3px;
                background: none;
                height: inherit;
                :hover {
                    cursor: pointer;
                    background: none;
                }
            }
        }
        & .dd-wrap {
            display: inline-block;
            margin-right: 20px;
        }
        
        & .ico {
            width: 20px;
            height: 20px;
            display: inline-block;
            margin-left: 10px;
            opacity: .8;
            background-size: contain;
            transition: opacity 0.3s ease;
            &: hover {
                opacity: 1;
                cursor: pointer;
            }
        }
         
        & .expander {
            position: absolute;
            top: 5px;
            right: 5px;
            width: 20px;
            background-repeat: no-repeat;
            height: 20px;
            :hover {
                cursor: pointer;
                background-color: ${props => props.theme.darkBgColor};
            }
            
            & .ico {
                margin: 0;
                margin: 2px 3px 7px;
                width: 15px;
                height: 15px;
            }
        }
        
       

`;

const Input = styled.input.attrs({
    type: 'text'
})`
    ${props => props.theme.components.inputsStyles}
    width: 200px;
    height: 35px;
    line-height: 35px;
    margin: 0;
    background-size: 20px;
    background-repeat: no-repeat;
    background-position: 95%;
    &:focus + label,  &:valid + label {
     ${props => props.theme.components.labelStyles.focused}
         transform: translate3d(-8px,-33px,0);
     
    }
    &:hover {
        cursor: pointer;
    }
`;

const FieldSet = styled.fieldset`
    ${props => props.theme.components.fieldSetStyles}
    display: inline-block;
    margin: 0;
`;

const Label = styled.label`
    ${props => props.theme.components.labelStyles.general};
    top: 6px;
`;


export {
    CtrlWrap,
    Input,
    FieldSet,
    Label
};