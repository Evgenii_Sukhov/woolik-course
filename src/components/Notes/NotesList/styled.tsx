import styled from 'styled-components';

export const Notes = styled.div`
    display: flex;
    justify-content: flex-start;
    flex-wrap: wrap;
`;