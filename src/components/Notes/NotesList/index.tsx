import * as React from "react";
import { default as Note} from "../Note";
import { INoteItem } from "../../../core/types";
import { Notes } from "./styled";

export interface IProps {
    notes: INoteItem[];
}

export default class NotesList extends React.Component<IProps, object> {
    public render() {

        return (
            <Notes>
            {this.props.notes.map((c: INoteItem, i) => (
                <Note key={i} note={c} />
            ))}
            </Notes>
        )
    }
}