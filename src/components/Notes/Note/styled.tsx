import styled from 'styled-components';

export const NoteItem = styled.div`
    text-align: left;
    font-size: 18px;
    line-height: 35px;
    color: #fff;
    cursor: pointer;
    transition: all 0.3s ease;
    background-color: #0002;
    margin-bottom: 20px;
    width: 250px;
    margin-right: 20px;
    height: 300px
    & .header {
        display: flex;
        align-items: flex-start;
        justify-content: flex-start;
        background-color: #0003;
        padding: 5px 10px;

    }
    
    & .body {
         padding: 10px 15px;
    }
    & h2 {
        font-family: cursive;
        font-style: italic;
    }
`;

export const NoteTags = styled.div`
    font-weight: 100;
    font-style: italic;
    text-transform: lowercase;
    font-size: 15px;
    margin-top: 10px;
`;