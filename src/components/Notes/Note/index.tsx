import * as React from 'react';
import { INoteItem } from "../../../core/types";
import { NoteItem, NoteTags } from "./styled";

export interface IProps {
    note: INoteItem;
}

export default class Note extends React.Component<IProps, object> {
    public render() {
        const {title, body, tags} = this.props.note;
        return (
            <NoteItem className={'note-item'}>
                <div className="header">NOTE</div>
                <div className="body">
                    <div>
                        <h2>{title}</h2>
                        <p>{body}</p>
                    </div>
                    <NoteTags>{
                        tags.map((c: string) => `#${c}`).join(', ')
                    }</NoteTags>
                </div>
            </NoteItem>
        )
    }
}