import * as React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({component: Component, ...rest}) => {
    const render = props => localStorage.getItem('_usr') ?  <Component {...props} /> :
        <Redirect to={{pathname: '/login', state: { from: props.location }}} />;

    return (<Route {...rest}
        render={render}
    />);
};

export default PrivateRoute;