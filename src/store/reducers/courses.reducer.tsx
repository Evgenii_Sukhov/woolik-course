import { coursesActionTypes } from '../actionTypes/coursesTypes';
import { ICourse } from '../../core/types';


export interface IState {
    list: ICourse[]
}

const initialState: IState = {
    list: []
};

export function courses(state = initialState, {type, payload}) {

    switch(type) {
        case coursesActionTypes.FETCH_COURSE_LIST_SUCCESS:
            return {
                ...state,
                list: payload
            };
        default:
            return state;
    }
}
