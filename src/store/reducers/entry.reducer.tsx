import { userLoginTypes, userRegisterTypes } from '../actionTypes/userTypes';

function getUserAuth() {
    const userData = localStorage.getItem('_usr');
    return userData ? JSON.parse(userData) : userData;
}

const userAuth = getUserAuth();
const initialState = {userAuth, code: ''};

export function entry(state = initialState, {type, payload}) {
    switch(type) {
        case userLoginTypes.LOGIN_SUCCEED:
            localStorage.setItem('_usr', JSON.stringify(payload));
            return {
                ...state,
                userAuth: payload
            };
        case userRegisterTypes.REGISTER_SUCCEED:
            return {
                ...state,
                code: payload.code
            };
        default:
            return state;
    }
}


