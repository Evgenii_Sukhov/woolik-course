import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { entry } from './entry.reducer';
import { courses } from './courses.reducer';
import { articles } from './articles.reducer.tsx';

export default combineReducers({
    entry,
    courses,
    articles,
    routing: routerReducer
})