import { IArticleItem } from '../../core/types';
import { articlesActionTypes } from '../actionTypes/articlesTypes';

export interface IState {
    list: IArticleItem[]
}

const initialState: IState = {
    list: []
};

export function articles(state = initialState, {type, payload}) {

    switch(type) {
        case articlesActionTypes.ARTICLES_SUCCEED:
            return {
                ...state,
                list: payload
            };
        default:
            return state;
    }
}
