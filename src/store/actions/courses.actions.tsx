import { coursesActionTypes } from '../actionTypes/coursesTypes';
import { getCourses } from '../../services/courses.service';

const getCourseList = () => async dispatch => {
    dispatch({type: coursesActionTypes.FETCH_COURSE_LIST_REQUEST});
    try {
        const courses = await getCourses();
        dispatch({
            type: coursesActionTypes.FETCH_COURSE_LIST_SUCCESS,
            payload: courses
        });
    } catch (error) {
        dispatch({
            type: coursesActionTypes.FETCH_COURSE_LIST_FAIL,
            error
        });
    }
};

export const coursesActions = {
    getCourseList
};