import { alertTypes }  from '../actionTypes/alertTypes';

const clear = () => ({ type: alertTypes.CLEAR });
const error = message => ({ type: alertTypes.ERROR, payload: message });
const success = message => ({ type: alertTypes.SUCCESS, payload: message });


export const alertActions = {
    clear,
    error,
    success
};