import { userLoginTypes, userRegisterTypes } from '../actionTypes/userTypes';
import { IUserRegisterFields } from '../../core/types';
import { login, register } from '../../services/entry.service';
export { userLoginTypes } from '../actionTypes/userTypes';

const loginUser = ({code, password}) => async dispatch => {
  dispatch({type: userLoginTypes.LOGIN_REQUESTED, payload: {code, password}});

  try {
      const user = await login({code, password});
      dispatch({type: userLoginTypes.LOGIN_SUCCEED, payload: user});
  } catch(error) {
      dispatch({
          type: userLoginTypes.LOGIN_FAILED,
          error
      });
  }
};

const registerUser = (registerData: IUserRegisterFields) => async dispatch => {
    dispatch({type: userRegisterTypes.REGISTER_REQUESTED, payload: registerData});
    try {
        const payload = await register(registerData);
        dispatch({
            type: userRegisterTypes.REGISTER_SUCCEED,
            payload
        });
    } catch(error) {
        dispatch({
            type: userRegisterTypes.REGISTER_FAILED,
            error
        });
    }


};

export const userActions = {
    loginUser,
    registerUser
};