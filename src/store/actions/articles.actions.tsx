import { articlesActionTypes } from '../actionTypes/articlesTypes';
import { getArticlesList } from '../../services/articles.service';

export const getArticles = () => async dispatch => {
    dispatch({type: articlesActionTypes.ARTICLES_REQUESTED});

    try {
        const articles = await getArticlesList();
        dispatch({type: articlesActionTypes.ARTICLES_SUCCEED, payload: articles});
    } catch(error) {
        dispatch({
            type: articlesActionTypes.ARTICLES_FAILED,
            error
        });
    }
};