export const articlesActionTypes = {
    ARTICLES_FAILED: '[ARTICLES] Articles failed',
    ARTICLES_REQUESTED: '[ARTICLES] Articles requested',
    ARTICLES_SUCCEED: '[ARTICLES] Articles succeed'
};