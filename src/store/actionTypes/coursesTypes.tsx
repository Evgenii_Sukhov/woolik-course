export enum coursesActionTypes {
    FETCH_COURSE_LIST_REQUEST= '[COURSES] Fetch course list requested',
    FETCH_COURSE_LIST_SUCCESS = '[COURSES] Fetch course list succeed',
    FETCH_COURSE_LIST_FAIL= '[COURSES] Fetch course list failed'
}