export const userLoginTypes = {
    LOGIN_FAILED: '[USER] Login failed',
    LOGIN_REQUESTED: '[USER] Login requested',
    LOGIN_SUCCEED: '[USER] Login succeed'
};

export const userRegisterTypes = {
    REGISTER_FAILED: '[USER] Register failed',
    REGISTER_REQUESTED: '[USER] Register requested',
    REGISTER_SUCCEED: '[USER] Register succeed'
};