export const getArticles = state => state.articles.list;
export const getFullArticle = state => state.articles.full;