import { makeRequest } from './api.service';


export function getArticlesList() {
    return makeRequest('articles')
        .then(res => {
            if (!res.ok) {
                throw new Error(res.statusText);
            } else {
                return res;
            }
        })
        .then(res => res.json())
        .then(articles => articles.data)
}