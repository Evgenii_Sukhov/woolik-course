import { makeRequest } from './api.service';


export function register(body) {
    return makeRequest('users/register', {method: 'POST', body: JSON.stringify(body)})
        .then(res => {
            if (!res.ok) {
                throw new Error(res.statusText);
            } else {
                return res;
            }
        })
        .then(res => res.json())
        .then(registerData => registerData.data)
}

export function login(body) {
    return makeRequest('users/authenticate', {method: 'POST', body: JSON.stringify(body)})
        .then(res => {
            if (!res.ok) {
                throw new Error(res.statusText);
            } else {
                return res;
            }
        })
        .then(res => res.json())
        .then(userData => userData.data)
}