import config from '../config.json';

interface IParams {
    body?: any,
    headers?: object,
    method: string,
    params?: any,
    queryParams?: object[]

}

export function makeRequest(endPoint, params?: IParams) {

    if(params && !params.headers) {
        params.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }
    return fetch(`${config.server_api_url}/${endPoint}`, {...params as RequestInit});
}