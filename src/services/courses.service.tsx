import { makeRequest } from './api.service';


export function getCourses() {
    return makeRequest('courses')
        .then(res => {
            if (!res.ok) {
                throw new Error(res.statusText);
            } else {
                return res;
            }
        })
        .then(res => res.json())
        .then(courses => courses.data)
}