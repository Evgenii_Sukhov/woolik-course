import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';
import { createStore, applyMiddleware } from 'redux';
import createHistory from 'history/createBrowserHistory';
import registerServiceWorker from './registerServiceWorker';
import { History } from 'history';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { ThemeProvider } from "styled-components";
import RootReducer from './store/reducers';
import 'node_modules/highlight.js/styles/ocean.css'
import './index.css';
import theme from './core/Theme';

import { LayoutContainer } from './components/LayoutContainer';
import PrivateRoute from './components/PrivateRoute';
import RegisterMainComponent from './components/EntryContainer/RegisterMainComponent';
import LoginMainComponent from './components/EntryContainer/LoginMainComponent';


const history: History  = createHistory();
const middlewares = [
    thunk
];

const store = createStore(RootReducer, composeWithDevTools(
    applyMiddleware(...middlewares)
));

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Switch>
                    <Route path="/register" component={RegisterMainComponent}/>
                    <Route path="/login" component={LoginMainComponent} />
                    <PrivateRoute path="/" component={LayoutContainer} />
                </Switch>
            </ConnectedRouter>
        </Provider>
    </ThemeProvider>,
  document.getElementById('root') as HTMLElement
);

registerServiceWorker();
