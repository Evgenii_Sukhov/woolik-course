export function authHeader(): object {
  const user = JSON.parse(localStorage.getItem('_usr') || '{}');
  return user.t ? {'Authorization': `Bearer ${user.t}`} : {};
}