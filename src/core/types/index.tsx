export type BuiltinTheme = 'vs' | 'vs-dark' | 'hc-black';

export interface INoteItem {
    body: string;
    title: string;
    tags: string[];
}

export interface INotesState {
    notes: INoteItem[];
}

export interface IDDItem {
    default: boolean;
    text: string;
    value: string;
}

export interface IUserRegisterFields {
    birthDate: string;
    confirmPassword: string;
    courseID: string[];
    email: string;
    firstName: string;
    lastName: string;
    phone: string;
    password: string;
}

export interface ICourse {
    createdDate: Date,
    id: string,
    courseID: string,
    label: string,
    className: string
}

export interface IArticleItem {
    id: string;
    title: string;
    createdAt: Date;
    createdAtRow: string;
    uniqueSlug: string;
    subtitle: string;
    readingTime: number;
    preview: IPreview;
}

interface IPreview {
    name: string;
    type: number;
    text: string;
    layout: number;
    metadata: IMetaData;
}

interface IMetaData {
    id: string;
    originalWidth: number;
    originalHeight: number;
    isFeatured: boolean
}

export interface Ilanguage {
    id: string,
    label: string
}

export interface ITheme {
    id: BuiltinTheme,
    label: string,
    className: string
}


