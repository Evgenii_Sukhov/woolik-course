import { ITheme } from './types';

export const WELCOME_TEXT = `Welcome on board new student! \n
Thank you for the registration! \n
Your register code has been sent on your email. \n
Please use the code to login in system.`;

export const BASIC_THEMES: ITheme[] = [
    {
        id: 'vs',
        label: 'Visual Studio',
        className: 'ico-vs'
    },
    {
        id: 'vs-dark',
        label: 'Visual Studio Code',
        className: 'ico-vscode'
    },
    {
        id: 'hc-black',
        label: 'Dark theme',
        className: 'ico-sublime'
    }
];