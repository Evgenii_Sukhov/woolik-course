import * as React from 'react';



export default class TypeWriterComponent extends React.Component<any, any> {

    public text = '';
    public state = {
        text: ''
    };

    public componentDidMount() {

        this.tick();
    }

    public tick() {
        const text =  this.props.text.substring(0, this.state.text.length + 1)
        this.setState({
            text
        });
        if(this.state.text.length === this.props.text.length) {
            return;
        }
        setTimeout(this.tick.bind(this), 100)
    }


    public render() {
        const {text} = this.state;
        return (<span>{text}</span>)
    }
}