import * as React from 'react';
import { Fragment } from 'react';
import { ModalContent } from './ModalContent';


class Modal extends React.Component<any, any>{

    public state = {
        isOpen: false
    };

    public modalNode;
    private x1 = 0;
    private y1 = 0;

    public componentDidMount() {
        this.setState({...this.state, children: this.props.children});
        document.addEventListener('keydown', this.clickOutside, false);
    }


    public componentWillUnmount() {
        document.removeEventListener('keydown', this.clickOutside, false);
    }

    public componentDidUpdate(prevProps, prevState) {
        if (this.props.open !== prevProps.open) {
            this.openCloseModal(this.props.open, this.props.onOpenCloseCallback);
        }
    }

    public openCloseModal(isOpen, callback?) {
        this.setState({
            ...this.state,
            isOpen
        }, () => callback ? callback(this.state.isOpen) : null);
    }

    public render() {
        const { isOpen } = this.state;
        const {
            children,
            backdrop = true,
            modalTitle = '',
            position='center',
            width='auto' } = this.props;
        this.closeModal = this.closeModal.bind(this);
        this.clickOutside = this.clickOutside.bind(this);
        this.onDragStart = this.onDragStart.bind(this);
        return (
            <Fragment>
                {!isOpen ||
                <ModalContent
                    position={position}
                    width={width}
                    modalRef={this.assignedRef}
                    modalTitle={modalTitle}
                    backdrop={backdrop}
                    children={children}
                    clickOutside={this.clickOutside}
                    onDragStart={this.onDragStart}
                    closeModal={this.closeModal}
                     />}
            </Fragment>
        )
    }

    private assignedRef = n => this.modalNode = n;
    private closeModal = () => this.openCloseModal(false, this.props.onOpenCloseCallback);

    private clickOutside = e => {
        if (!this.props.closeOnClickOutside) { return; }
        e.stopPropagation();
        if(e.key && e.key === "Escape") {
            this.closeModal();
            return;
        }
        if (this.modalNode && this.modalNode.contains(e.target)) { return; }
        this.closeModal();
    };

    private onDragStart = e => {
        if (!this.props.draggable) { return; }
        document.onmouseup = this.closeDragElement;
        e.persist();
        this.x1 = e.clientX;
        this.y1 = e.clientY;
        document.onmousemove = event => this.elementDrag(event);
    };

    private closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }

    private elementDrag(event) {
        event = event || window.event;
        event.preventDefault();
        const x = this.x1 - event.clientX;
        const y = this.y1 - event.clientY;
        this.x1 = event.clientX;
        this.y1 = event.clientY;
        this.modalNode.style.top = `${this.modalNode.offsetTop - y}px`;
        this.modalNode.style.left = `${this.modalNode.offsetLeft - x}px`;

    }
}

export default Modal;