import styled from 'styled-components';
export const ModalWrapper: any = styled.div`
    ${props => props.theme.components.modalWrapper}
    .modal-container {
        width: ${props => (props as any).modalWidth};
        background: ${props => props.theme.mainBg}
        border: 1px solid ${props => props.theme.borderColor}
        box-shadow: ${props => props.theme.modalShadow}
        .modal-header {
            display: flex;
            align-items: center;
            flex-wrap: wrap;
            font-size: 14px;
            user-select: none;
            font-family: Times New Roman;
            padding: 7px 10px;
            justify-content: space-between;
            color: ${props => props.theme.mainBg};
            ${props => props.theme.bodyBackground}
            .ico {
                display: inline-block;
                width: 20px;
                height: 20px;
                cursor: pointer;
                color: #fff;
            }  
        }
        .modal-content {
            padding: 15px 10px;
        }
    }
`;