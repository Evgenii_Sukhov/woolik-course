import * as React from 'react'
import * as ReactDOM from 'react-dom';
import { ModalWrapper } from './styled';

export const ModalContent =
    ({
         children,
         backdrop,
         modalTitle,
         modalRef,
         position,
         clickOutside,
         onDragStart,
         width,
         closeModal
    }) =>
        <>
            {ReactDOM.createPortal(
        <ModalWrapper
            onClick={clickOutside}
            className={backdrop ? 'overlay' : ''}
            modalWidth={width}>
                    <div className={`modal-container ${position}`} ref={modalRef}>
                        <div className="modal-header" onMouseDown={onDragStart}>
                            <div className="modal-title">{modalTitle}</div>
                            <div className="modal-actions"><span onClick={closeModal} className="ico ico-close" /></div>
                        </div>
                        <div className="modal-content">
                            {children}
                        </div>
                    </div>
                </ModalWrapper>,
                document.body
            )}
        </>;
