import * as React from 'react';
import { Input } from './styled';

interface IProps {
    minLength?: number,
    levels?: string[],
    component?: React.Component,
    onChange: () => string,
    id?: string,
    name: string,
    className?: string
}

export class StrengthPassword extends React.Component<IProps, any> {

    public render() {
        const {id, name, onChange, className} = this.props;
        return (
            <>
                {this.getComponent()}
                <Input
                    className={className}
                    id={id} name={name}
                    onChange={onChange}
                    type="password"
                    required={true} />
            </>
        )
    }

    private getComponent() {
        return this.props.component || (
            <div className="strength-metric" />
        );
    }
}