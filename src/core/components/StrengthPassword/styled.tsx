import styled from 'styled-components';

const Input = styled.input.attrs({
    type: 'text' || 'password' || 'phone' || 'tel'
})`
    ${props => props.theme.components.inputsStyles}
    font-size: 1em;
    margin-bottom: 0;
    &:focus + label,  &:valid + label {
        ${props => props.theme.components.labelStyles.focused}
    }
`;

export {
    Input
}