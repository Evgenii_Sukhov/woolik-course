import * as React from "react";
import { IDDItem } from "../../types";
import {Ul, Val, Wrap} from "./styled";

export interface IDDList {
    items: IDDItem[];
    label: string;
    callback: (i: IDDItem) => void;
}

export default class DropDown extends React.Component<IDDList, object> {
    public state = {
        on: true,
    };

    public toggle = () => {
        this.setState({
            ...this.state,
            on: !this.state.on
        });
    };

    public pick = (i) => () => {
        this.props.callback(i);
    }

    public render() {
        const { items, label } = this.props;

        return (
            <Wrap onClick={this.toggle} className="dd-wrap ico-down">
                <Val>{label || 'All'}</Val>
                <Ul>
                <ul>
                    {items.map((c,i) => (
                        <li
                            style={{height: this.state.on ? '0px' : '50px'}}
                            onClick={this.pick(c)}
                            key={i}>{c.text}</li>
                    ))}
                </ul>
            </Ul>
            </Wrap>
        )
    }
}
