import styled from 'styled-components';


const Ul = styled.div`
        color:  ${p => p.theme.mainColor};
        overflow: hidden;
        background-color: #8EBAD5;
        position: absolute;
        top 100%;
        text-align: left;
        transition: height 0.3s ease;
        height: auto !important;
        width: 100%;
        left: 0;

        & li {
            padding: 0 10px;
            transition: all 0.3s ease;
            
            &: hover {
                padding-left: 10px;
                font-weight: bolder;
                font-size 120%;
                transition: all 0.3s ease;
            }
        }
`;

const Val = styled.div`
        color:  ${p => p.theme.mainColor};
        display: inline-block;
`;

const Wrap = styled.div`
    background-color: ${p=>p.theme.darkBgColor};
    position: relative;
    height: 35px;
    line-height: 35px;
    padding: 0 10px;
    width: 200px;
    text-align: left;
    background-size: 15px;
    background-repeat: no-repeat;
    background-position: 95%;
    &:hover {
        cursor: pointer;
    }
`;


export {
    Ul,
    Val,
    Wrap
};