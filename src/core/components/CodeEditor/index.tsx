import * as monaco from 'monaco-editor';
import * as React from 'react';
import Highlight from 'react-highlight'
import BuiltinTheme = monaco.editor.BuiltinTheme;
import { CodeEditorWrapper } from './styled';

export default class CodeEditor extends React.Component<any, any> {
    public container;
    public editor;
    public state = {
        results: ''
    };

    public componentDidMount() {
        this.initEditor();
    }

    public componentDidUpdate(prevProps) {
        if (
            this.editor &&
            (this.props.options.width !== prevProps.options.width || this.props.options.height !== prevProps.options.height)
        ) {
            this.editor.layout();
        }

        if (this.props.processName === 'run' &&
            prevProps.processName !== this.props.processName) {
            this.compileValue(this.editor);
        }

        if (prevProps.options.language !== this.props.options.language) {
            monaco.editor.setModelLanguage(this.editor.getModel(), this.props.options.language);
        }


        if (prevProps.options.theme !== this.props.options.theme) {
            monaco.editor.setTheme(this.props.options.theme);
        }
    }

    public componentWillUnmount() {
        this.destroyEditor();
    }

    public render() {
        return (
            <CodeEditorWrapper>
                <div ref={this.assignRef} style={{height: '400px', margin: '0 auto', fontFamily: 'Times New Roman'}} />
                {this.props.options.language === 'javascript' && this.props.showCompilePanel &&
                <Highlight language={this.props.options.language}>{this.state.results}</Highlight>}
            </CodeEditorWrapper>
        )
    }

    private initEditor() {
        const defaultOptions = {
            language: 'javascript',
            theme: 'vs-dark' as BuiltinTheme
        };
        if (this.container) {
            this.editor = monaco.editor.create(
                this.container,
                {
                    ...defaultOptions,
                    ...this.props.options,

                });
            this.editorChange(this.editor);
        }
    }

    private assignRef = component => {
        this.container = component;
    };

    private toJSON(obj) {
        return JSON.stringify(obj, (key, value) => {
            if (typeof value === 'function') {
                return ` [Function: ${key}]`;
            } else {
                return value;
            }
        });
    }

    private editorChange(editor) {
        editor.onDidChangeModelContent(event => {
            if (!this.props.autoRun) { return; }
            this.compileValue(editor);
        });
    }

    private compileValue(editor) {
        const value = editor.getValue();
        try {
            const res = eval(value);
            if (res instanceof Function) {
                this.setState({results:  `[Function: ${res.name}]`});
                return;
            } else if(res instanceof Object) {
                this.setState({results: this.toJSON(res).replace(/"/g, '')})
                return;
            }
            this.setState({results: res})

        } catch (e) {
            this.setState({results: `[ERROR] ${e.message}`});
        }
        this.props.onFinishManualCompile();
    }

    private destroyEditor() {
        if (typeof this.editor !== 'undefined') {
            this.editor.dispose();
        }
    }
}