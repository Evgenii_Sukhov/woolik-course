import styled from 'styled-components';

const CodeEditorWrapper = styled.div`
    &>* {max-width: 850px; margin: 0 auto;}
`;

export {
    CodeEditorWrapper
}