import styled from 'styled-components';

const SelectOption = styled.div`
    ${props => props.theme.components.selectOptionStyles};
    .select-ico {
        background-size: cover;
        margin-left: 10px;
    }
`;

const Fieldset = styled.fieldset`
    border: none;
    padding-bottom: 10px;
    &>label {
        font-size: 14px;
        font-family: Times New Roman;
        padding: 3px 0;
        line-height: 24px;
    }
    ${props => props.theme.components.customCheckbox};
`;

export {
    SelectOption,
    Fieldset
}