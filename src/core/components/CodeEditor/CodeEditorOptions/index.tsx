import * as React from 'react';
import Select from 'react-select'
import { Fieldset, SelectOption } from './styled';

export function CodeEditorOptions (
    {
        languages,
        themes,
        handleLanguageSelectChange,
        handleThemeSelectChange,
        selectedLanguage,
        selectedTheme,
        showCompilePanel,
        handleShowCompileChange,
        autoRun,
        handleAutoRunChange
    }) {

    const getOptionValue = option => option.id;
    const getTheme = theme => ({
        ...theme,
        borderRadius: 0,
        spacing: {
            ...theme.spacing,
            controlHeight: 45
        }
    });

    function Option({data, innerProps}) {
        const className = `select-ico ${data.className}`;
        return (
            <SelectOption {...innerProps}>
                <div className={className} />
                <div className="option-caption">{data.label}</div>
            </SelectOption>
        )
    }
    return (
        <form>
            <Fieldset>
                <label htmlFor="control-language-select">
                    Programming language
                </label>
                <Select
                    components={{Option}}
                    theme={getTheme}
                    options={languages}
                    value={selectedLanguage}
                    getOptionValue={getOptionValue}
                    placeholder="Choose programming language"
                    id="control-language-select"
                    onChange={handleLanguageSelectChange}
                />
            </Fieldset>
            <Fieldset>
                <label htmlFor="control-theme-select">
                    Editor theme
                </label>
                <Select
                    components={{Option}}
                    theme={getTheme}
                    options={themes}
                    value={selectedTheme}
                    getOptionValue={getOptionValue}
                    placeholder="Choose editor theme"
                    id="control-theme-select"
                    onChange={handleThemeSelectChange}
                />
            </Fieldset>
            {
                selectedLanguage.id === 'javascript' &&
                    <Fieldset className="custom-checkbox">
                        <input type="checkbox" id="control-compile-checkbox" onChange={handleShowCompileChange} checked={showCompilePanel} />
                        <label htmlFor="control-compile-checkbox">
                            Show compile panel
                        </label>
                    </Fieldset>
            }
            {
                selectedLanguage.id === 'javascript' && showCompilePanel &&
                <Fieldset className="custom-checkbox">
                    <input type="checkbox" id="control-run-checkbox" onChange={handleAutoRunChange} checked={autoRun} />
                    <label htmlFor="control-run-checkbox">
                        Auto run JS code
                    </label>
                </Fieldset>
            }
        </form>
    )
}