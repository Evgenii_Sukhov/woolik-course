import styled from 'styled-components';

const EditorControlWrapper = styled.div`
    ${props => props.theme.components.controlsWrapperStyles}
    padding: 10px;
    color:  ${props => props.theme.shadowColor};
    background-color: ${props => props.theme.mainElemBg};
    z-index: 1;
    font-size: 13px;
    .option-caption {
        cursor: pointer;
        padding: 5px 10px
        color: #000;
    }
    ul {
        display: flex;
        li {
            cursor: pointer;
            margin: 0 5px;
            .ico {
                width: 20px;
                height: 20px;
            }
        }
        
    }
`;


export {
    EditorControlWrapper
}