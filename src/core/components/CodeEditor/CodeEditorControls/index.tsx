import * as React from 'react';
import { EditorControlWrapper } from './styled';

export function CodeEditorControls({
                                       onClickOpenSettings,
                                       selectedLanguage,
                                       showCompilePanel,
                                       onClickCompileCode
                                   }) {
    return (
        <EditorControlWrapper>
            <ul>
                <li title='Editor settings' onClick={onClickOpenSettings}>
                    <span className="ico ico-setting" />
                </li>
                {
                    selectedLanguage.id === 'javascript' && showCompilePanel &&
                    <li title='Run JS' onClick={onClickCompileCode}>
                        <span className="ico ico-play" />
                    </li>

                }
            </ul>
        </EditorControlWrapper>
    )
}