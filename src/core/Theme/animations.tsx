import { keyframes } from 'styled-components';

const roundRotation = keyframes`
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
`;

const fadeIn = keyframes`
    0% {
        opacity: 0.9;
    }
    100% {
        opacity: 1;
    }  
`;

const rotateX= keyframes`
    0%   { transform: rotateX(45deg); }
    25%   { transform: rotateX(60deg); }
    50%   { transform: rotateX(80deg); }
    100% { transform: rotateX(0); }
`;

const rotateYSmall= keyframes`
    0%   { transform: rotateY(0deg); }
    20%   { transform: rotateY(35deg); }
    40%   { transform: rotateY(45deg); }
    100% { transform: rotateY(0); }
`;


const rotateZ= keyframes`
    0%   { transform: rotateY(-45deg); }
    25%   { transform: rotateY(-60deg); }
    50%   { transform: rotateY(-80deg); }
    100% { transform: rotateY(0); }
`;


const zoom = keyframes`
    0% {
        transform: scale(1);
    }
    50% {
        transform: scale(1.2);
    }
    100% {
        transform: scale(1.4);
    }
`;

const waveRotation = keyframes`
    0% {
        transform: rotate(-45deg);
    }
    10% {
        transform: rotate(45deg);
    }
    20% {
        transform: rotate(-35deg);
    }
    30% {
        transform: rotate(35deg);
    }
    40% {
        transform: rotate(-25deg);
    }
    50% {
        transform: rotate(25deg);
    }
    70% {
        transform: rotate(-15deg);
    }
    80% {
        transform: rotate(15deg);
    }
    100% {
        transform: rotate(0deg);
    }
`;

const collapse = keyframes`
    0% {
        right: 110px;
        top: 20px;
    }
    10% {
        height: 100px;
        width: 50px;
        right: 111px;
        top: 21px;
        padding: 0;
    }
    20% {
        height: 20px;
        width: 20px;
    }
    50% {
        height: 26px;
        width: 26px;
    }
    100% {
        top: calc(100% - 30px);
        right: calc(100% - 30px);
        height: 26px;
        width: 26px;
    }
`;

const rize = keyframes`
    0% {
        top: calc(100% - 30px);
        right: calc(100% - 30px);
        height: 26px;
        width: 26px;
        overflow: hidden;
    }
    70% {
        right: 130px;
        top: 30px;
        height: auto;
        width: auto;
        overflow: hidden; 
    }
    100% {
        height: auto;
        width: auto;
        overflow: visible; 
    }
`;


export default {
    collapse,
    fadeIn,
    rize,
    rotateX,
    rotateYSmall,
    rotateZ,
    roundRotation,
    waveRotation,
    zoom,

};