const inputsStyles = `
    -webkit-transition: all 0.30s ease-in-out;
    -moz-transition: all 0.30s ease-in-out;
    -ms-transition: all 0.30s ease-in-out;
    -o-transition: all 0.30s ease-in-out;
    outline: none;
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    width: 100%;
    background-color: #fff;
    margin-bottom: 4%;
    border: 1px solid #ccc;
    padding: 3%;
    color: #555;
    appearance: none;
    
    &:focus {
        box-shadow: 0 0 8px #1b94f5d6;
        padding: 3%;
        outline: 0;
        border: 1px solid #1b94f5d6;
    }
`;

const labelStyles = {
    focused: `
        font-size: 14px;
        color: #fff;
        transform: translate3d(-8px, -38px, 0);
    `,
    general: `
        position: absolute;
        top: 12px;
        left: 8px;
        color: #020202c9;
        transform: translate3d(0, 0, 0);
        transition: all 0.2s ease-in-out;
    `
};

const controlsWrapperStyles = `
    padding: 10px 40px 10px 10px;
    position: relative;
    transition: opacity 0.3s ease;
    position: fixed;
    width: auto;
    right: 130px;
    top: 30px;
`;

const fieldSetStyles = `
    position: relative;
    margin: 20px 0 0;
    padding: 0;
    border: 0;
`;

const buttonsStyles = `
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    width: 100%;
    padding: 3%;
    font-size: 14px;
    background: #0000005c;
    border-top-style: none;
    border-right-style: none;
    border-left-style: none;   
    border-bottom-style: none;  
    color: #fff;
    
    &:hover {
        background: #00000057;
        cursor: pointer;
    }
`;

const selectOptionStyles = `
    
    padding: 5px 0;
    &:hover {
        background: #44a7ec;
        color: white;
    }
    & div {
        display: inline-block;
        padding-left: 5px;
        cursor: pointer;
    }
    .select-ico {
        width: 22px;
        height: 22px;
        padding-right: 5px;
    }
`;

const modalWrapper = `
    
    &.overlay {
        background: #00000099;
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        display: flex;  
        justify-content: center;
        align-items: center;
    }
    
    & .modal-container {
        position: absolute;
        transform: translate(-50%, -50%);
        z-index: 2;
        &.center {
            top: 50%;
            left: 50%;
        }
    }
    
    
`;

const customCheckbox = `
    &.custom-checkbox {
        input[type="checkbox"] {
            position: absolute;
            opacity: 0;
            
            & + label {
                position: relative;
                cursor: pointer;
                padding: 0;
            }
            & + label:before {
                content: '';
                margin-right: 10px;
                display: inline-block;
                vertical-align: text-top;
                width: 20px;
                height: 20px;
                background: white;
                border: 1px solid #068ddc;
            }
            // &:checked + label:before {
            //     background: #068ddc;
            // }
            &:checked + label:after {
                content: '✓';
                position: absolute;
                left: 3px;
                top: 0;
                color: #068ddc;
                font-size: 21px;
                font-weight: bold;
            }
        }
`;

const pageWrapper = `
    font-family: Monaco, Menlo, monospace;
    max-height: 100vh;
    padding: 30px;
    width: calc(100% - 100px);
    & h1 {
        color:  #fff;
        font-family: cursive;
        font-style: italic;
        height: 50px;
        line-height: 50px;
    }
    
    & header {
        display: flex;
        justify-content: space-between;
        align-items: flex-start;
        margin-bottom: 60px;
    }
`;



export default {
    buttonsStyles,
    controlsWrapperStyles,
    fieldSetStyles,
    inputsStyles,
    labelStyles,
    selectOptionStyles,
    modalWrapper,
    customCheckbox,
    pageWrapper
}

