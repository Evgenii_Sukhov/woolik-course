import animations from './animations';
import components from './components';

export default {
    animations,
    components,
    darkBgColor: '#0003',
    mainBg: '#fff',
    mainColor: '#fff',
    mainElemBg: '#0000001f',
    shadowColor: '#84A8D1',
    errorMsgColor: '#ff0303',
    iconOnWhite: '#007fff',
    bodyBackground: `background: rgb(135, 224, 253); /* Old browsers */
    background: -moz-linear-gradient(top, rgb(4, 181, 239) 0%,rgb(21, 166, 212) 40%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgb(4, 181, 239) 0%,rgb(21, 166, 212) 40%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgb(4, 181, 239) 0%,rgb(21, 166, 212) 40%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#87e0fd', endColorstr='#79e0bf', GradientType=0); /* IE6-9 */
    `,
    gradientColor: `
        linear-gradient(to bottom, rgba(135, 224, 253, 1) 0%, rgba(83, 203, 241, 1) 40%);
    `,
    borderColor: '#068ddc',
    modalShadow: '0 0 10px 0px #00000096;'
};

